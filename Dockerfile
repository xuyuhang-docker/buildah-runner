FROM registry.gitlab.com/xuyuhang-docker/alpine:3.14.1
WORKDIR /root

RUN apk add --update git bash openssh-client curl buildah \
    rm -rf /var/cache/apk/*
